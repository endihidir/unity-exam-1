﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextElement : UIElement {

    [SerializeField] private Text _text;

    protected override void Show(ActionData data)
    {
        if (_text) _text.enabled = true;
    }

    protected override void Hide(ActionData data)
    {
        if (_text) _text.enabled = false;
    }
}