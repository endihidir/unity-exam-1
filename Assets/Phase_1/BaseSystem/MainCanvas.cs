﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCanvas : MonoBehaviour {

    public static MainCanvas Instance { get; private set; }
    public RectTransform Root => _root;
    [SerializeField] private RectTransform _root;
	// Use this for initialization
	void Awake ()
	{
	    Instance = this;
	}
}
