﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : ManagableObject
{
    [SerializeField] private List<StateAction> _actions;
    [SerializeField] private int _enterActionId = 100;
    [SerializeField] private int _exitActionId = 200;

    void Awake()
    {
        DownSideAction += TriggerAction;
    }

    private void TriggerAction(int actionId, ActionData data)
    {
        foreach (var action in _actions)
        {
            if (action.TryTrigger(actionId, data))
            {
                TriggerExit(data);
                return;
            }
        }
    }

    private void TriggerExit(ActionData data)
    {
        TriggerUpside(_exitActionId, data);
    }

    protected void TriggerEnter(ActionData data)
    {
        TriggerUpside(_enterActionId, data);
    }

    [Serializable]
    public class StateAction
    {
        [SerializeField] private string _name;
        [Header("If (ActionIds.isEmpty || ActionIds.Contains(ActionId) State.Enter(Data);")]
        [SerializeField] private List<int> _actionIds;

        [SerializeField] private State _targetState;

        public bool TryTrigger(int actionId, ActionData data)
        {
            if (_targetState && _actionIds.Count == 0 || _actionIds.Contains(actionId))
            {
                _targetState.TriggerEnter(data);
                return true;
            }
            return false;
        }
    }
}