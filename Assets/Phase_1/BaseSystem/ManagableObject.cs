using System;
using UnityEngine;

public abstract class ManagableObject : MonoBehaviour
{
    protected event Action<int, ActionData> UpSideAction;
    protected event Action<int, ActionData> DownSideAction;
    protected void TriggerUpside(int actionId, ActionData data) => UpSideAction?.Invoke(actionId, data);
    protected void TriggerDownside(int actionId, ActionData data) => DownSideAction?.Invoke(actionId, data);
    
    protected static void Connect(ManagableObject up, ManagableObject down)
    {
        down.UpSideAction += up.TriggerUpside;
        up.DownSideAction += down.TriggerDownside;
    }
}