using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class ActionData
{
    public readonly string Data;
    public readonly Type DataType;

    public ActionData(string data, Type type)
    {
        Data = data;
        DataType = type;
    }

    public ActionData(Object serializableObject, Type type)
    {
        Data = JsonUtility.ToJson(serializableObject);
        DataType = type;
    }

    public void Deserialize(object objectToOverride)
    {
        JsonUtility.FromJsonOverwrite(Data, objectToOverride);
    }

}