﻿using System;

public class PageInstancable : Page
{
    [NonSerialized] private bool _attached = false;
    public bool Attach(State state)
    {
        if (_attached || Awaken || !SetState(state)) return false;
        Awake();
        return true;
    }
}